
require(['jquery','owlCarousel'], function($) {
    $(document).ready(function(){
        $('.owl-carousel').owlCarousel({
            items: 1,
            smartSpeed: 1000,
            navSpeed: 1000,
            // autoplay: true,
            // autoplayTimeout: 6000,
            autoplayHoverPause: true,
            loop: true,
            dots: true,
            nav: true
        });

        $('.owl-carousel').on('changed.owl.carousel', function(e) {
            if (!e.namespace || e.property.name != 'position') return
            console.log(e.item.index + '/' + e.item.class);
        })
    });
});