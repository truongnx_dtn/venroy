var config = {
    // map: {
    //     '*': {
    //         'owlCarousel': 'js/jquery/owl.carousel'
    //     }
    // },
    paths: {
        'owlCarousel': 'js/jquery/owl.carousel'
    },

    shim: {
    	'owlCarousel': {
    		'deps': ['jquery']
    	}
    }
};
