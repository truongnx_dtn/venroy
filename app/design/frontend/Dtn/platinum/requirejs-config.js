var config = {
    // map: {
    //     '*': {
    //         'owlCarousel': 'js/jquery/owl.carousel'
    //     }
    // },
    paths: {
        'jCarousel': 'js/jcarousel/jquery.jcarousel.min'
    },

    shim: {
    	'jCarousel': {
    		'deps': ['jquery']
    	}
    }
};
